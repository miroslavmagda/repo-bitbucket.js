/**
 * @mekwall's .vangogh() for Syntax Highlighting
 *
 * All code is open source and dual licensed under GPL and MIT.
 * Check the individual licenses for more information.
 * https://github.com/mekwall/jquery-vangogh/blob/master/GPL-LICENSE.txt
 * https://github.com/mekwall/jquery-vangogh/blob/master/MIT-LICENSE.txt
 */
(function($,a,b){var c=1,d=!1,e=!1,f={get:function(b){var c=a.location.hash;if(c.length>0){var d=c.match(new RegExp(b+":{([a-zA-Z0-9,-]*)}"));if(d)return d[1].split(",")}return[]},set:function(b,c){var d=a.location.hash,e,f=b+":{"+c.join(",")+"}",g=d.indexOf(b+":{");if(c.length===0)return this.remove(b);g!==-1?e=d.replace(new RegExp("("+b+":{[a-zA-Z0-9,-]*})"),f):e=d.length>0?d+","+f:f,a.location.hash=e},remove:function(b){a.location.hash=a.location.hash.replace(new RegExp("([,]?"+b+":{[a-zA-Z0-9,-]*}[,]?)"),"")}},g={numberRange:/^([0-9]+)-([0-9]+)$/,pageNumber:/-([0-9]+)$/,multilineBegin:/<span class="([\w-_][^"]+)">(?:.[^<]*(?!<\/span>)|)$/ig,multilineEnd:/(<span class="([\w-_][^"]+)">)?(?:.[^<]*)?(<\/span>)/ig};$.fn.vanGogh=function(h){function n(){if(d||e)setTimeout(n,100);else{k++;if(k>=10)return;if(h.source&&!l){e=!0,$.ajax({url:h.source,crossDomain:!0,dataType:"text",success:function(a){l=a},error:function(a,b){l="ERROR: "+b},complete:function(){e=!1,n()}});return}b=b||a.hljs;if(!b){d=!0,$.getScript(h.autoload,function(){d=!1,n()});return}j.filter("pre,code").each(function(){function r(b,c,e){var h=!1,i=a.find(".vg-line");c&&(a.find(".vg-highlight").removeClass("vg-highlight"),f.remove(d),k=[]),b=$.type(b)==="array"?b:[b],$.each(b,function(b,c){if(k.indexOf(c)<=-1){!isNaN(parseFloat(c,10))&&isFinite(c)&&(c=parseInt(c,10));if($.type(c)==="string"){var e=g.numberRange.exec(c);if(e){var f=e[1],h=e[2],j="";for(var b=f;b<=h;b++)j+=",#"+d+"-"+b,k.push(b);i.filter(j.substring(1)).addClass("vg-highlight")}else a.find(".vg-line:contains("+c+")").each(function(){var a=$(this).addClass("vg-highlight");a.html(a.html().replace(c,'<span class="vg-highlight">'+c+"</span>"))}),k.push(c)}else{var l=d+"-"+this,m=i.filter("#"+l);m.length&&(m.addClass("vg-highlight"),k.push(c))}}}),!e&&f.set(d,k)}var a=$(this).addClass("vg-container").attr("id",this.id||"vg-"+c++),d=this.id,e=a.find("code"),i=!1,j=!1,k=[];e.length||(e=a,i=!0),h.source&&l&&e.text(l);var n=e.text();b.highlightBlock(e[0],h.tab);var o=e.html().split("\n"),p="",q="";if(!i){var s={},t=0;$.each(o,function(a,b){var c=a+h.firstLine,e=d+"-"+c,f=b;h.numbers&&(p+='<a class="vg-number" rel="#'+e+'">'+c+"</a>");if(s[t]){var i=g.multilineEnd.exec(b);i&&!i[1]?(f='<span class="'+s[t]+'">'+f,delete s[t],t--):f='<span class="'+s[t]+'">'+f+"</span>"}var j=g.multilineBegin.exec(b);j&&(t++,s[t]=j[1]),q+='<div class="vg-line" id="'+e+'">'+f+"</div>"}),q='<code class="vg-code">'+q+"</code>",h.numbers&&(q='<div class="vg-gutter">'+p+"</div>"+q),a.html(q),e=a.find("code"),a.find(".vg-number").click(function(b){var c=$(this),e=c.attr("rel"),h=a.find(e);if(h.hasClass("vg-highlight")){h.removeClass("vg-highlight"),k.splice(k.indexOf(c.text()),1),f.set(d,k),j=!1;return!1}var i=j;j=parseInt(g.pageNumber.exec(e)[1],10),b.shiftKey&&j?r(i<j?i+"-"+j:j+"-"+i,!0):r(j,b.ctrlKey?!1:!0);return!1});var u=a.find(".vg-gutter"),v=u.outerWidth(),w=0,x=!1;h.animateGutter&&a.scroll(function(a){if(this.scrollLeft!==w)if(this.scrollLeft>v){if(this.scrollLeft<w)w=this.scrollLeft,u.hide();else if(this.scrollLeft!==w){if(x)return;var b=this;w=this.scrollLeft,x=setTimeout(function(){x=!1;var a=b.scrollLeft;e.css("marginLeft",v),u.css({"float":"none",position:"absolute",left:a-v}).show().stop().animate({left:a})},500)}}else w=this.scrollLeft,clearTimeout(x),x=!1,u.css({"float":"",position:"",left:""}).show()})}else i&&a.addClass("vg-code");e.dblclick(function(){m(e[0]);return!1});if(h.maxLines>0){var y=a.find(".vg-line").height(),z=parseInt(e.css("paddingTop")),A=y*(h.maxLines+1)+z;a.css({minHeight:y+z,maxHeight:A})}h.highlight&&r(h.highlight,!0,!0);var B=f.get(d);B.length&&r(B,!1,!0)})}}function m(b){var c=a,d=a.document;if(d.body.createTextRange){var e=d.body.createTextRange();e.moveToElementText(b),e.select()}else if(d.createRange){var f=c.getSelection(),e=d.createRange();e.selectNodeContents(b),f.removeAllRanges(),f.addRange(e)}}var i={language:"auto",firstLine:1,maxLines:0,numbers:!0,highlight:null,animateGutter:!0,autoload:"http://softwaremaniacs.org/media/soft/highlight/highlight.pack.js",tab:"    "};h=$.extend({},i,h);var j=this,k=0,l;n();return j}})(jQuery,this,typeof this.hljs!="undefined"?this.hljs:!1);


/**
 * Repo-bitbucket.js jQuery plugin
 * v 0.9
 */
(function($) {
	$.fn.extend({
		bitbucket : function(options) {
			//file extension
			var ext = {
				png : 'image',
				gif : 'image',
				jpg : 'image',
				jpeg : 'image',
				zip : 'bin',
				war : 'bin'
			};

			var settings = $.extend({
				user : '',
				repository : '',
				revision : 'master'
			}, options);
			var ui = {};
			var css = '';
			var folders = [];
			var objId = '';
			var d = document;

			ui.prepare = function(obj) {
				$('body').prepend($('<style id="bitbucket_style_css">').html(css));
				obj.attr('class', 'bitbucket_css');
			};
			ui.header = function(text) {
				text = (text) ? text : '';
				$('.bitbucket_css .bitbucket_css_header').html('<span>' + text + '</span>');
			};
			ui.error = function(text, title) {
				title = (title) ? title : 'Error';
				text = (text) ? text : 'Unknown error...';
				$('.bitbucket_css .bitbucket_css_header').html('<span>Error</span>');
				$('.bitbucket_css .bitbucket_css_content').html('<span></span>');
			};
			var createFolderLink = function(directory) {
				//https://bitbucket.org/m/img/icons/fugue/folder.png
				var link = $(d.createElement("a"));
				link.attr('href', 'javascript:');
				link.attr('x-path', directory);
				link.attr('x-name', directory);
				link.attr('class', 'dir');
				link.html(directory);
				$('#' + objId + ' .bitbucket_css_content').append(link);
				link.click(function(e) {
					var elm = $(e.target);
					load.folder(elm.attr('x-path'));
				});
			};
			var createFileLink = function(file) {
				//https://bitbucket.org/m/img/icons/fugue/document_text.png
				var link = $(d.createElement("a"));
				link.attr('href', 'javascript:');
				link.attr('x-path', file.path);
				link.attr('x-name', file.name);
				link.attr('class', 'file');
				link.html(file.name);
				$('#' + objId + ' .bitbucket_css_content').append(link);
				link.click(function(e) {
					var elm = $(e.target);
					var fileExt = elm.attr('x-name').split('.').pop();
					switch(ext[fileExt]) {
						case 'image':
							load.image(elm.attr('x-path'));
							break;
						case 'bin':
							load.bin(elm.attr('x-path'));
							break;
						default:
							load.file(elm.attr('x-path'));
					}
				});
			};
			var load = {};
			load.folder = function(folder) {
				if (folder) {
					folders.push(folder);
				} else {
					folders = [];
				}
				var url = 'https://api.bitbucket.org/1.0/repositories/' + settings.user + '/' + settings.repository + '/src/' + settings.revision + '/' + folders.join('/');
				$.ajax({
					url : url,
					type : 'GET',
					data : {},
					cache : false,
					dataType : 'jsonp',
					objId : objId,
					success : function(response) {
						$('#' + objId + ' .bitbucket_css_content').empty();
						var link = $(d.createElement("a"));
						link.attr('href', 'javascript:');
						link.html(settings.repository + ' / ' + folders.join(' / '))
						link.click(function() {
							load.folder();
						});
						$('.bitbucket_css .bitbucket_css_header span').empty();
						$('.bitbucket_css .bitbucket_css_header span').append(link);
						for (var i = 0; i < response.directories.length; i++) {
							var directory = response.directories[i];
							createFolderLink(directory);
						}
						for (var i = 0; i < response.files.length; i++) {
							var file = response.files[i];
							file.name = file.path.replace(response.path, '');
							createFileLink(file);
						}

					},
					error : function(response) {
						ui.error('No repository found...');
					}
				});
			};
			load.image = function(path) {
				var url = 'https://api.bitbucket.org/1.0/repositories/' + settings.user + '/' + settings.repository + '/raw/' + settings.revision + '/';
				$('#' + objId + ' .bitbucket_css_content').html('<img src="' + url + path + '">');
			};
			load.bin = function(path) {
			};
			load.file = function(path) {
				var url = 'https://api.bitbucket.org/1.0/repositories/' + settings.user + '/' + settings.repository + '/src/' + settings.revision + '/';
				$.ajax({
					url : url + path,
					type : 'GET',
					data : {},
					cache : false,
					dataType : 'jsonp',
					success : function(response) {
						var pre = $(d.createElement("pre"));
						var code = $(d.createElement("code"));
						//code.attr("css","json");
						code.text(response.data).html();
						pre.html(code);
						$('#' + objId + ' .bitbucket_css_content').html(pre);
						pre.vanGogh();
					},
					error : function() {
						$('#' + objId + ' .bitbucket_css_content').html('Cannot show file from repository...');
					}
				});
			};
			return this.each(function() {
				var obj = $(this);
				objId = obj.attr('id');
				obj.html('<div class="bitbucket_css_header"></div><div class="bitbucket_css_content"></div>');
				ui.prepare(obj);
				ui.header('Loading repository...');
				load.folder();

			});
		}
	});
})(jQuery);

